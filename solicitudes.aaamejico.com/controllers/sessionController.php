<?php
    ini_set('display errors',1);
    ini_set('display_startup_errors',1);

    error_reporting(E_ALL);

    class SessionController {

        public function __construct() {
            session_start();

            if (!isset($_SESSION['token'])) {
                $_SESSION['token'] = md5(uniqid(mt_rand(),true));
            }
        }

        public function setCurrentUser($name) {

            $_SESSION['name'] = $name;
        }

        public function getName() {

            return $_SESSION['name'];
        }

        public function terminate() {

            if (ini_get("session.use_cookies")) {
                $params = session_get_cookie_params();
                setcookie(session_name(), '', time() - 42000,
                    $params["path"], $params["domain"],
                    $params["secure"], $params["httponly"]
                );
            }
            
            session_unset();
            session_destroy();
        }

        public function getToken() {
            return $_SESSION['token'];
        }

        public function login($user,$pass,$conn){

            if (!$conn->connect_error) {

                $query = "SELECT * FROM Employee WHERE emp_user = ?";
                $prepared_query = $conn->prepare($query);
                $prepared_query->bind_param('s',$user);
                
                if($prepared_query->execute()) {

                    $results = $prepared_query->get_result();
                    $users = $results->fetch_all(MYSQLI_ASSOC);

                    if (count($users) > 0) {
                        foreach ($users as $user) {

                            if ($user) {
                                if(password_verify($pass,$user['emp_password'])) {
                                    
                                    $this->setCurrentUser($user['emp_name'],$user['emp_lname'],$user['emp_grade'],$user['emp_level']);
                                    echo json_encode(array('success' => 'Sesión iniciada'));
                                } else {
                                    echo json_encode(array('error' => 'Verifique su contraseña'));
                                }
                            }
                        }
                    } else {
                        echo json_encode(array('error' => 'Verifique su nombre de usuario'));
                    }
                    
                } else {
                    echo json_encode(array('Error al obtener datos'));
                }
            } else {
                return json_encode(array('Error de conexión'));
            }
        }
    }
?>