<?php

    require_once("connection.php");
    require_once("sessionController.php");

    $sess = new SessionController();

    if (isset($_POST)) {
        
        $conn = connect();

        if(!$conn->connect_error){

            $user = $_POST["user"];
            $pass = $_POST["pass"];

            if($user != "" && $pass != "") {

                $query = "SELECT * FROM users WHERE username = ?";
                $prepared_query = $conn->prepare($query);
                $prepared_query->bind_param('s',$user);

                if($prepared_query->execute()) {

                    $results = $prepared_query->get_result();
                    $users = $results->fetch_all(MYSQLI_ASSOC);

                    if(password_verify($pass,$users[0]["password"])) {

                        $sess->setCurrentUser("PUERK");

                        echo "Iniciando sesión...";
                    } else {
                        echo "Error en la contraseña, inténtalo de nuevo";
                    }
                } else {
                    echo "Error al enviar los datos";
                }
            } else {
                echo "Hay un error en los datos enviados, revisa y envía el formulario de nuevo";
            }
        } else {

            echo "Parece que tuvimos un error de conexión, inténtalo más tarde.";
        }
    }

?>