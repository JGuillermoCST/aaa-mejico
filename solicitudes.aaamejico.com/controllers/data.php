<?php

    require_once("connection.php");

    class QuotesController {

        function get() {

            $conn = connect();

            if (!$conn->connect_error) {

                $query = "SELECT * FROM quotes ORDER BY id DESC";
                $prepared_query = $conn->prepare($query);
                $prepared_query->execute();

                $results = $prepared_query->get_result();
                $quotes = $results->fetch_all(MYSQLI_ASSOC);

                if($quotes) {
                    return $quotes;
                } else {
                    return array();
                }
            } else {
                return array();
            }
        }
    }
?>