<header>
    <div class="h-container">
        <div class="h-title">
            <div class="h-brand">
                <div class="h-brand-img">
                    <a href="https://aaamejico.com">
                        <img src="img_tile" alt="Business tile logo">
                    </a>
                </div>
                <div class="h-brand-title">
                    <h1 class="h-brand-title-tx">AAA <span>méjico</span></h1>
                </div>
            </div>

            <div class="h-brand brand-second">
                <div class="h-brand-title">
                    <h1 class="h-brand-title-tx">A<span>rquitectura</span> A<span>ntigua de las</span> A<span>méricas</span></h1>
                </div>
                <div class="h-brand-img">
                    <img src="img_symbol" alt="Business symbol logo">
                </div>
            </div>
        </div>
        
        <nav id="h-nav" class="h-nav">
            <a id="index" href="https://aaamejico.com" class="option">Inicio</a>
            <a id="albums" href="https://aaamejico.com/albumes" class="option">Álbumes</a>
            <a id="about" href="https://aaamejico.com/acerca" class="option">Acerca de</a>
            <a id="contact" href="https://aaamejico.com/contacto" class="option">Contacto</a>
            <a id="rstate" href="https://aaamejico.com/inmobiliaria" class="option">AAA inmobiliaria</a>
            <a class="icon" onclick="myFunction()">
                <i class="fa fa-bars"></i>
            </a>
        </nav>
    </div>
</header>