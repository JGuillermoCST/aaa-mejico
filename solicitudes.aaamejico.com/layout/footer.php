<footer>
    <div class="f-container">
        <div class="f-dev">
            <img src="img_dev" alt="Developer brand logo">
            <div class="f-dev-info">
                <p>Designed and developed by:<br>José Guillermo Valenzuela Castro<br>Software development leader at Hacker’s Rooftop<br>December 2020</p>
                <div class="f-dev-networks">
                    <a href="http://linkedin.com/in/JGuillermoCST">
                        <img src="img_dev_in" alt="Developer Linked In">
                    </a>
                    <a href="http://facebook.com/JGuillermoCST">
                        <img src="img_dev_fb" alt="Developer Facebook">
                    </a>
                    <a href="https://www.joseguillermocst.com">
                        <img src="img_dev_web" alt="Developer Web">
                    </a>
                </div>
            </div>
        </div>
        <div class="f-brand">
            <p>Arquitectura Antigua de las Américas<br>Inmobiliaria y Construcciones</p>
            <a href="https://aaamejico.com">
                <img src="img_tile" alt="Business tile logo">
            </a>
        </div>
        <div class="f-brand-data">
            <p>Dirección:<br>Calle Madero 91, Centro Histórico<br>Guadalajara 44100, Jalisco, Méjico</p>
            <p>Contacto:<br>informes@aaamejico.com</p>
        </div>
    </div>
</footer>