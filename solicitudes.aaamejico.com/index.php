<!DOCTYPE html>

<html lang="es">
    <head>
        <?php include_once("layout/tags.php"); ?>
        <meta name="title" content="AAA méjico">
        <meta name="description" content="AAA Méjico es una empresa Inmobiliaria que se dedica al asesoramiento en la compra y venta de Bienes Raíces operando desde 1996">

        <?php include_once("layout/stylesheets.php"); ?>
        <link rel="stylesheet" href="st_index">
        
        <title>Inicio</title>
    </head>
    <body>
        <div class="container-fluid">
            <?php include_once("layout/header.php"); ?>

            <main>
                <div class="m-container">
                    <div class="m-top">
                        <h1>Iniciar sesión</h1>
                    </div>
                    <div class="m-form">
                        <form id="login" class="row g-3" method="POST">
                            <div class="col-12">
                                <label for="i-user" class="form-label">Usuario</label>
                                <input type="text" class="form-control" id="i-user">
                            </div>
                            <div class="col-12">
                                <label for="i-pass" class="form-label">Contraseña</label>
                                <input type="password" class="form-control" id="i-pass">
                            </div>
                            <div class="col-12">
                                <button type="submit" class="btn btn-primary">Sign in</button>
                            </div>
                        </form>
                    </div>
                </div>
            </main>

            <?php include_once("layout/footer.php"); ?>
        </div>
        <?php include_once("layout/scripts.php"); ?>
        <script src="sc_index"></script>
    </body>
</html>