$('#login').on('submit',function(evt) {

    evt.preventDefault()

    let name = $('#i-user').val()
    let pass = $('#i-pass').val()

    $.ajax({

        type: "POST",
        url: "controllers/session.php",
        data: {user: name,
                pass: pass},
        success: function (response) {
            swal(response)

            if (response == 'Iniciando sesión...') {
                
                setTimeout(function() {

                    window.location.href = "registros"
                }, 1200)
            }
        },
        error: function () {
            swal("Error al intentar establecer conexión")
        }
    })
})