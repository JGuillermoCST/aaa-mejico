<!DOCTYPE html>

<?php 
    require_once("../controllers/sessionController.php");
    require_once("../controllers/data.php");
    $sess = new SessionController();
    $data = new QuotesController();

    $regs = $data->get();
?>

<html lang="es">
    <head>
        <?php include_once("../layout/tags.php"); ?>
        <meta name="title" content="AAA méjico">
        <meta name="description" content="AAA Méjico es una empresa Inmobiliaria que se dedica al asesoramiento en la compra y venta de Bienes Raíces operando desde 1996">

        <?php include_once("../layout/stylesheets.php"); ?>
        <link rel="stylesheet" href="st_index">
        
        <title>Inicio</title>
    </head>
    <body>
        <div class="container-fluid">
            <?php include_once("../layout/header.php"); ?>

            <main>
                <div class="m-container">
                    <div class="m-top">
                        <h1>Registros</h1>
                    </div>
                    <div class="m-data-table">
                        <table id="studentUpdateInfo" class="table table-striped table-bordered bg-white mt-3">
                            <thead>
                                <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Correo</th>
                                <th scope="col">Asunto</th>
                                <th scope="col">Area</th>
                                <th scope="col">Rango</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if($regs): ?>
                                    <?php foreach ($regs as $r):?>
                                        <tr>
                                            <td scope="row"><?= $r['id'] ?></td>
                                            <td> <?= $r['name'] ?> </td>
                                            <td> <?= $r['email'] ?> </td>
                                            <td> <?= $r['kind'] ?> </td>
                                            <td> <?= $r['area'] ?> </td>
                                            <td> <?= $r['p_range'] ?> </td>
                                        </tr>
                                    <?php endforeach ?>
                                <?php endif ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </main>

            <?php include_once("../layout/footer.php"); ?>
        </div>
        <?php include_once("../layout/scripts.php"); ?>
        <script src="sc_index"></script>
    </body>
</html>