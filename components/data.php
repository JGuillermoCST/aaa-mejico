<?php

    require_once("connection.php");

    if (isset($_POST)) {
        
        $conn = connect();

        if(!$conn->connect_error){

            $name = $_POST["name"];
            $email = $_POST["email"];
            $kind = $_POST["kind"];
            $area = $_POST["area"];
            $range = $_POST["range"];

            if($name != "" && $email != "" && $kind != "" && $area != "" && $range != "") {

                $query = "INSERT INTO quotes (name,email,kind,area,p_range) values (?,?,?,?,?)";
                $prepared_query = $conn->prepare($query);
                $prepared_query->bind_param('sssss',$name,$email,$kind,$area,$range);

                if($prepared_query->execute()) {

                    echo "Datos enviados! Pronto estaremos en contacto contigo.";
                } else {
                    echo "Error al enviar los datos";
                }
            } else {
                echo "Hay un error en los datos enviados, revisa y envía el formulario de nuevo";
            }
        } else {

            echo "Parece que tuvimos un error de conexión, inténtalo más tarde.";
        }
    }

?>