<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="language" content="Spanish">
<meta name="keywords" content="inmobiliaria, guadalajara, jalisco, mexico, mejico, aaa, propiedades, construccion, bienes, raices, compra, venta, remodelacion">
<meta name="robots" content="index, follow">
<meta name="revisit-after" content="7 days">
<meta name="author" content="Jose Guillermo V. Castro">

<link rel="preconnect" href="https://fonts.gstatic.com"> 
<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300&display=swap" rel="stylesheet">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
<link  rel="icon"   href="img_icon"/>
