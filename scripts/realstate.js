$('#rstate').addClass('active')

$('#quotation').on('submit',function(evt) {

    evt.preventDefault()

    let name = $('#i-name').val()
    let email = $('#i-email').val()
    let kind = $('#i-kind').val()
    let area = $('#i-area').val()
    let range = $('#i-range').val()

    $.ajax({

        type: "POST",
        url: "../components/data.php",
        data: {name: name,
                email: email,
                kind: kind,
                area: area,
                range: range},
        success: function (response) {
            swal(response)
        },
        error: function () {
            swal("Error al intentar establecer conexión")
        }
    })
})