<!DOCTYPE html>

<html lang="es">
    <head>
        <?php include_once("../layout/tags.php"); ?>
        <meta name="title" content="AAA méjico">
        <meta name="description" content="AAA Méjico es una empresa Inmobiliaria que se dedica al asesoramiento en la compra y venta de Bienes Raíces operando desde 1996">

        <?php include_once("../layout/stylesheets.php"); ?>
        <link rel="stylesheet" href="st_albums">

        <title>Álbumes</title>
    </head>
    <body>
        <div class="container-fluid">
            <?php include_once("../layout/header.php"); ?>

            <main>
                <div class="m-container">

                    <!-- 2022 ALBUM INIT -->
                    <div class="m-album">
                        <div class="m-album-snippet">
                            <h3>Restauración de piezas (Nuevo)</h3>
                            <a href="album1-22">Ver álbum</a>
                        </div>
                        <img src="img_a1_22_portrait" alt="Album 1-22 image">
                    </div>
                    <div class="m-album">
                        <div class="m-album-snippet">
                            <h3>Cambios de espacios (Nuevo)</h3>
                            <a href="album2-22">Ver álbum</a>
                        </div>
                        <img src="img_a2_22_portrait" alt="Album 2-22 image">
                    </div>
                    <div class="m-album">
                        <div class="m-album-snippet">
                            <h3>Áreas exteriores (Nuevo)</h3>
                            <a href="album3-22">Ver álbum</a>
                        </div>
                        <img src="img_a3_22_portrait" alt="Album 3-22 image">
                    </div>

                    <div class="m-album">
                        <div class="m-album-snippet">
                            <h3>Objetos y antigüedades P1 (Nuevo)</h3>
                            <a href="album4-22">Ver álbum</a>
                        </div>
                        <img src="img_a4_22_portrait" alt="Album 4-22 image">
                    </div>

                    <div class="m-album">
                        <div class="m-album-snippet">
                            <h3>Objetos y antigüedades P2 (Nuevo)</h3>
                            <a href="album5-22">Ver álbum</a>
                        </div>
                        <img src="img_a5_22_portrait" alt="Album 5-22 image">
                    </div>

                    <div class="m-album">
                        <div class="m-album-snippet">
                            <h3>Objetos y antigüedades P3 (Nuevo)</h3>
                            <a href="album6-22">Ver álbum</a>
                        </div>
                        <img src="img_a6_22_portrait" alt="Album 6-22 image">
                    </div>
                    <!-- 2022 ALBUM END -->

                    <div class="m-album">
                        <div class="m-album-snippet">
                            <h3>Construcción de terraza techada</h3>
                            <a href="album1">Ver álbum</a>
                        </div>
                        <img src="img_a1_portrait" alt="Album 1 image">
                    </div>
                    <div class="m-album">
                        <div class="m-album-snippet">
                            <h3>Cocinas</h3>
                            <a href="album2">Ver álbum</a>
                        </div>
                        <img src="img_a2_portrait" alt="Album 2 image">
                    </div>
                    <div class="m-album">
                        <div class="m-album-snippet">
                            <h3>Fachadas y exteriores</h3>
                            <a href="album3">Ver álbum</a>
                        </div>
                        <img src="img_a3_portrait" alt="Album 3 image">
                    </div>

                    <div class="m-album">
                        <div class="m-album-snippet">
                            <h3>Remodelaciones de acuerdo al estilo</h3>
                            <a href="album4">Ver álbum</a>
                        </div>
                        <img src="img_a4_portrait" alt="Album 4 image">
                    </div>
                    <div class="m-album">
                        <div class="m-album-snippet">
                            <h3>Techos de teja</h3>
                            <a href="album5">Ver álbum</a>
                        </div>
                        <img src="img_a5_portrait" alt="Album 5 image">
                    </div>
                    <div class="m-album">
                        <div class="m-album-snippet">
                            <h3>Interiores</h3>
                            <a href="album6">Ver álbum</a>
                        </div>
                        <img src="img_a6_portrait" alt="Album 6 image">
                    </div>

                    <div class="m-album">
                        <div class="m-album-snippet">
                            <h3>Varios</h3>
                            <a href="album7">Ver álbum</a>
                        </div>
                        <img src="img_a7_portrait" alt="Album 7 image">
                    </div>
                </div>
            </main>

            <?php include_once("../layout/footer.php"); ?>
        </div>
        <?php include_once("../layout/scripts.php"); ?>
        <script src="sc_albums"></script>
    </body>
</html>