<!DOCTYPE html>
<html lang="es">
    <head>
        <?php include_once("../layout/tags.php"); ?>
        <meta name="title" content="AAA méjico">
        <meta name="description" content="AAA Méjico es una empresa Inmobiliaria que se dedica al asesoramiento en la compra y venta de Bienes Raíces operando desde 1996">

        <?php include_once("../layout/stylesheets.php"); ?>
        <link rel="stylesheet" href="st_about">

        <title>Sobre nosotros</title>
    </head>
    <body>
        <div class="container-fluid">
            <?php include_once("../layout/header.php"); ?>

            <main>
                <div class="m-container">
                    <div class="m-title">
                        <h1>Acerca de nosotros</h1>
                    </div>
                    <div class="m-info">
                        <p>Arquitectura Antigua de las Américas  (AAA mejico) es una empresa que opera desde 1996 en diferentes municipios del Estado de Jalisco y Guadalajara.<br><br>Nuestra finalidad es adaptar las construcciones o remodelaciones con el máximo aprovechamiento de los espacios disponibles y la mayúscula sensación de belleza en cada lugar de la finca.<br><br>Arquitectura Antigua de las Américas más que una constructora es una empresa que se dedica a crear belleza para el beneficio del propietario, su familia e invitados.</p>
                        <img src="img_contract" alt="Contract image">
                    </div>
                </div>
            </main>

            <?php include_once("../layout/footer.php"); ?>
        </div>
        <?php include_once("../layout/scripts.php"); ?>
        <script src="sc_about"></script>
    </body>
</html>