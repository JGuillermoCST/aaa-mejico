<!DOCTYPE html>

<html lang="es">
    <head>
        <?php include_once("../layout/tags.php"); ?>
        <meta name="title" content="AAA méjico">
        <meta name="description" content="AAA Méjico es una empresa Inmobiliaria que se dedica al asesoramiento en la compra y venta de Bienes Raíces operando desde 1996">

        <?php include_once("../layout/stylesheets.php"); ?>
        <link rel="stylesheet" href="st_contact">

        <title>Contacto</title>
    </head>
    <body>
        <div class="container-fluid">
            <?php include_once("../layout/header.php"); ?>

            <main>
                <div class="m-container">
                    <div class="m-title">
                        <h1>Contacto</h1>
                    </div>
                    <div class="m-info">
                        <div class="m-info-map">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3732.9038576233766!2d-103.34703018526037!3d20.673490084465897!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8428b1f0e6ba6257%3A0xb3d70d6d4ba7d905!2sCalle%20Francisco%20I.%20Madero%2091%2C%20Zona%20Centro%2C%2044100%20Guadalajara%2C%20Jal.!5e0!3m2!1sen!2smx!4v1609670486789!5m2!1sen!2smx" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                        </div>
                        <div class="m-info-tx">
                            <div class="m-info-tx-direction">
                                <p>Dirección:<br>Calle Madero 91<br>Centro Histórico<br>Guadalajara 44100, Jalisco<br>México</p>
                            </div>
                            <div class="m-info-tx-contact">
                                <p>Contacto:<br>informes@aaamejico.com<br><br><br><br>Horario:<br>L-V: 09:00 - 20:00</p>
                            </div>
                        </div>
                    </div>
                </div>
            </main>

            <?php include_once("../layout/footer.php"); ?>
        </div>
        <?php include_once("../layout/scripts.php"); ?>
        <script src="sc_contact"></script>
    </body>
</html>