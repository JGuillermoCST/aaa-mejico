<!DOCTYPE html>

<html lang="es">
    <head>
        <?php include_once("../../layout/tags.php"); ?>
        <meta name="title" content="AAA méjico">
        <meta name="description" content="AAA Méjico es una empresa Inmobiliaria que se dedica al asesoramiento en la compra y venta de Bienes Raíces operando desde 1996">

        <?php include_once("../../layout/stylesheets.php"); ?>
        <link rel="stylesheet" href="st_album5">

        <title>Álbum 5</title>
    </head>
    <body>
        <div class="container-fluid">
            <?php include_once("../../layout/header.php"); ?>

            <main>
                <div class="m-container">
                    <div class="m-info">
                        <div class="m-info-title">
                            <h1>Techos de teja</h1>
                        </div>
                        <div class="m-info-description">
                            <p>Aquí en primer lugar el punto era definir los espacios techados y la construcción de las columnas.  Después se instaló el piso de ladrillo bien recocido y colocado en de una forma que tuviera un dibujo diferente. Los polines de madera y la fajilla de madera son de pino secado en horno y tratado contra la polilla.  Aquí el proceso de instalación de polines y fajillas. Por último se coloca la teja comenzando de abajo hacia arriba.</p>
                        </div>
                    </div>
                    <div class="m-gallery">
                        <div class="m-gal-row1">
                            <img class="img5-1" src="img_a5.1" alt="album5-image1">
                            <img class="img5-2" src="img_a5.2" alt="album5-image2">
                            <img class="img5-3" src="img_a5.3" alt="album5-image3">
                        </div>

                        <div class="m-gal-row2">
                            <img class="img5-4" src="img_a5.4" alt="album5-image4">
                            <img class="img5-5" src="img_a5.5" alt="album5-image5">
                            <img class="img5-6" src="img_a5.6" alt="album5-image6">
                        </div>

                        <div class="m-gal-row3">
                            <img class="img5-7" src="img_a5.7" alt="album5-image7">
                            <img class="img5-8" src="img_a5.8" alt="album5-image8">
                            <img class="img5-9" src="img_a5.9" alt="album5-image9">
                        </div>
                    </div>
                </div>
            </main>

            <?php include_once("../../layout/footer.php"); ?>

            <div id="img-viewer" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <img class="img-modal" src="" alt="image-display" style="width: 100%">
                    </div>
                </div>
            </div>
        </div>
        <?php include_once("../../layout/scripts.php"); ?>
        <script src="sc_album5"></script>
    </body>
</html>
