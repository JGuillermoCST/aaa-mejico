<!DOCTYPE html>

<html lang="es">
    <head>
        <?php include_once("../../layout/tags.php"); ?>
        <meta name="title" content="AAA méjico">
        <meta name="description" content="AAA Méjico es una empresa Inmobiliaria que se dedica al asesoramiento en la compra y venta de Bienes Raíces operando desde 1996">

        <?php include_once("../../layout/stylesheets.php"); ?>
        <link rel="stylesheet" href="st_album7">

        <title>Álbum 7</title>
    </head>
    <body>
        <div class="container-fluid">
            <?php include_once("../../layout/header.php"); ?>

            <main>
                <div class="m-container">
                    <div class="m-info">
                        <div class="m-info-title">
                            <h1>Varios</h1>
                        </div>
                        <div class="m-info-description">
                            <p>Este álbum es para presentar otros diferentes trabajos como una recámara con una columna iluminada; la construcción de una fuente en un patio, también pintado con barro color arcilla; elementos decorativos en un departamento; un lavamanos con un ovalín sobrepuesto en esa barra de madera de caoba; la fachada de una casa con techo de teja; una cabecera de ladrillo aparente; una fuente de cantera dentro de una casa y por último la ambientación en un patio mexicano.</p>
                        </div>
                    </div>
                    <div class="m-gallery">
                        <div class="m-gal-sub1">
                            <img class="img7-1" src="img_a7.1" alt="album7-image1">
                            <img class="img7-2" src="img_a7.2" alt="album7-image2">
                            <img class="img7-3" src="img_a7.3" alt="album7-image3">
                            <img class="img7-4" src="img_a7.4" alt="album7-image4">
                        </div>

                        <div class="m-gal-sub2">
                            <img class="img7-5" src="img_a7.5" alt="album7-image5">
                            <img class="img7-6" src="img_a7.6" alt="album7-image6">
                            <img class="img7-7" src="img_a7.7" alt="album7-image7">
                            <img class="img7-8" src="img_a7.8" alt="album7-image8">
                        </div>
                    </div>
                </div>
            </main>

            <?php include_once("../../layout/footer.php"); ?>

            <div id="img-viewer" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <img class="img-modal" src="" alt="image-display" style="width: 100%">
                    </div>
                </div>
            </div>
        </div>
        <?php include_once("../../layout/scripts.php"); ?>
        <script src="sc_album7"></script>
    </body>
</html>