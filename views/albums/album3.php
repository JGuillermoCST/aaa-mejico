<!DOCTYPE html>

<html lang="es">
    <head>
        <?php include_once("../../layout/tags.php"); ?>
        <meta name="title" content="AAA méjico">
        <meta name="description" content="AAA Méjico es una empresa Inmobiliaria que se dedica al asesoramiento en la compra y venta de Bienes Raíces operando desde 1996">

        <?php include_once("../../layout/stylesheets.php"); ?>
        <link rel="stylesheet" href="st_album3">

        <title>Álbum 3</title>
    </head>
    <body>
        <div class="container-fluid">
            <?php include_once("../../layout/header.php"); ?>

            <main>
                <div class="m-container">
                    <div class="m-info">
                        <div class="m-info-title">
                            <h1>Fachadas y exteriores</h1>
                        </div>
                    </div>
                    <div class="m-gallery">
                        <div class="m-gal-sub1">
                            <div class="m-sub-description">
                                <h3>Construcción de fachada</h3>
                                <p>En este caso la fachada de la casa estaba descuadrada, pintada de varios colores, con una ventana demasiado pequeña que servía para iluminación y aireación del baño y la puerta principal era de metal. Al final se alinearon los dos muros en uno solo, se construyó una nueva y más amplia ventana para el baño, se consiguió una antigua puerta de madera, se le construyó un marco y se pintó esa fachada.</p>
                            </div>
                            <div class="m-sub-img">
                                <img class="img3-1" src="img_a3.1" alt="album3-image1">
                                <img class="img3-2" src="img_a3.2" alt="album3-image2">
                            </div>
                        </div>

                        <div class="m-gal-sub2">
                            <div class="m-sub-description">
                                <h3>Organización de patio</h3>
                                <p>En medio de este patio se encontraba una fuente que restaba espacio útil. Lo único que se hizo en este caso es retirar la vieja fuente y construir otra sobre el muro. De esta manera se logró obtener amplitud y más sentido del orden.</p>
                            </div>
                            <div class="m-sub-img">
                                <img class="img3-3" src="img_a3.3" alt="album3-image3">
                                <img class="img3-4" src="img_a3.4" alt="album3-image4">
                            </div>
                        </div>

                        <div class="m-gal-sub3">
                            <div class="m-sub-description desc3">
                                <h3>Construcción de jardín y terraza techada</h3>
                                <p>En un espacio en la parte trasera de una casa se construyó un espacio verde y terraza. El terreno era más alto que el nivel de la casa así que se tuvo que bajar parte de éste para llevarlo al nivel requerido. Posteriormente se construyeron unas escaleras para acceder a la sección más alta donde también se le construyó un hidromasaje para 6 personas en forma de pila rústica. Se instalaron losetas de barro, se construyó muro de retención de piedra, se agregaron pasto, macetas y plantas. Para la terraza se construyeron columnas para sostener el tejabán fabricado con polines de madera y teja. Al fondo a la izquierda en ambas imágenes se puede apreciar un grueso tronco de un mango como referencia de que se trata del mismo espacio.</p>
                            </div>
                            <div class="m-sub-img">
                                <img class="img3-5" src="img_a3.5" alt="album3-image5">
                                <img class="img3-6" src="img_a3.6" alt="album3-image6">
                            </div>
                        </div>

                        <div class="m-gal-sub4">
                            <div class="m-sub-description">
                                <h3>Patio con fuente</h3>
                                <p>En este patio se desmontó la fuente que se encontraba en medio. Una fuente de cantera que no es de la región y que además resta espacio útil al patio. Al final se construyó otra fuente sobre un muro, se instaló piso de ladrillo bien recocido, jardinería, macetas e iluminación. El resultado es más estético y con mucho más orden.</p>
                            </div>
                            <div class="m-sub-img">
                                <div class="m-sub-img-bef">
                                    <img class="img3-7" src="img_a3.7" alt="album3-image7">
                                    <img class="img3-8" src="img_a3.8" alt="album3-image8">
                                </div>
                                <div class="m-sub-img-aft">
                                    <img class="img3-9" src="img_a3.9" alt="album3-image9">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>

            <?php include_once("../../layout/footer.php"); ?>

            <div id="img-viewer" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <img class="img-modal" src="" alt="image-display" style="width: 100%">
                    </div>
                </div>
            </div>
        </div>
        <?php include_once("../../layout/scripts.php"); ?>
        <script src="sc_album3"></script>
    </body>
</html>