<!DOCTYPE html>

<html lang="es">
    <head>
        <?php include_once("../../layout/tags.php"); ?>
        <meta name="title" content="AAA méjico">
        <meta name="description" content="AAA Méjico es una empresa Inmobiliaria que se dedica al asesoramiento en la compra y venta de Bienes Raíces operando desde 1996">

        <?php include_once("../../layout/stylesheets.php"); ?>
        <link rel="stylesheet" href="st_album4">

        <title>Álbum 4</title>
    </head>
    <body>
        <div class="container-fluid">
            <?php include_once("../../layout/header.php"); ?>

            <main>
                <div class="m-container">
                    <div class="m-info">
                        <div class="m-info-title">
                            <h1>Remodelaciones de acuerdo al estilo</h1>
                        </div>
                        <div class="m-info-description">
                            <p>Cuando se nos pide cambiar la fisonomía o distribución de alguno de los espacios de una finca, creemos que es importante considerar el ejecutar o repetir una expresión constructiva acorde a los materiales ya existentes, respetar las formas y la ambientación de acuerdo a la época de su edificación, entre otros; más que una remodelación pareciese una especie de restauración con el fin de que cada detalle termine integrado en el conjunto total.<br><br>En este ejemplo, este salón fue alterado en varias ocasiones a través de los siglos, nuestro cliente nos pidió ampliar el vano de una puerta y se decidió construir un arco de medio punto incorporándolo en esos gruesos muros de adobe fabricados hace más de 200 años. Acordamos retirar el aplanado (enjarre de muros) para dejar descubierto el adobe, se insertó una dala antes de abrir el vano para evitar el colapso del adobe. La construcción del arco se realizó con ladrillo recocido en cimbra. Finalmente con polvo de adobe se elaboró una mezcla para recubrir los ladrillos de barro y así integrarlo estéticamente al mismo color del adobe de toda el área.</p>
                        </div>
                    </div>
                    <div class="m-gallery">
                        <div class="row1">
                            <img class="img4-1" src="img_a4.1" alt="album4-image1">
                            <img class="img4-2" src="img_a4.2" alt="album4-image2">
                            <img class="img4-3" src="img_a4.3" alt="album4-image3">
                        </div>

                        <div class="row2">
                            <img class="img4-4" src="img_a4.4" alt="album4-image4">
                            <img class="img4-5" src="img_a4.5" alt="album4-image5">
                            <img class="img4-6" src="img_a4.6" alt="album4-image6">
                            <img class="img4-7" src="img_a4.7" alt="album4-image7">
                        </div>
                    </div>
                </div>
            </main>

            <?php include_once("../../layout/footer.php"); ?>

            <div id="img-viewer" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <img class="img-modal" src="" alt="image-display" style="width: 100%">
                    </div>
                </div>
            </div>
        </div>
        <?php include_once("../../layout/scripts.php"); ?>
        <script src="sc_album4"></script>
    </body>
</html>