<!DOCTYPE html>

<html lang="es">
    <head>
        <?php include_once("../../layout/tags.php"); ?>
        <meta name="title" content="AAA méjico">
        <meta name="description" content="AAA Méjico es una empresa Inmobiliaria que se dedica al asesoramiento en la compra y venta de Bienes Raíces operando desde 1996">

        <?php include_once("../../layout/stylesheets.php"); ?>
        <link rel="stylesheet" href="st_album1-22">

        <title>Restauración de piezas</title>
    </head>
    <body>
        <div class="container-fluid">
            <?php include_once("../../layout/header.php"); ?>

            <main>
                <div class="m-container">
                    <div class="m-info">
                        <div class="m-info-title">
                            <h1>Restauración de piezas</h1>
                        </div>
                        <div class="m-info-description">
                            <p>Se restauran piezas a solicitud del cliente ya sea en papel, lienzo, escultura en madera, etc.</p>
                        </div>
                    </div>
                    <div class="m-gallery">
                        <div class="m-gal-sub1">
                            <img class="img1-1" src="img_a1.1-22" alt="album1-image1">
                        </div>

                        <div class="m-gal-sub2">
                            <div class="m-sub2-row1">
                                <img class="img1-2" src="img_a1.2-22" alt="album1-image2">
                                <img class="img1-3" src="img_a1.3-22" alt="album1-image3">
                            </div>
                            <div class="m-sub2-row2">
                                <img class="img1-4" src="img_a1.4-22" alt="album1-image4">
                                <img class="img1-5" src="img_a1.5-22" alt="album1-image5">
                            </div>
                        </div>
                    </div>
                </div>
            </main>

            <?php include_once("../../layout/footer.php"); ?>

            <div id="img-viewer" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <img class="img-modal" src="" alt="image-display" style="width: 100%">
                    </div>
                </div>
            </div>
        </div>
        <?php include_once("../../layout/scripts.php"); ?>
        <script src="sc_album1-22"></script>
    </body>
</html>