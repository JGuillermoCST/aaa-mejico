<!DOCTYPE html>

<html lang="es">
    <head>
        <?php include_once("../../layout/tags.php"); ?>
        <meta name="title" content="AAA méjico">
        <meta name="description" content="AAA Méjico es una empresa Inmobiliaria que se dedica al asesoramiento en la compra y venta de Bienes Raíces operando desde 1996">

        <?php include_once("../../layout/stylesheets.php"); ?>
        <link rel="stylesheet" href="st_album2">

        <title>Álbum 2</title>
    </head>
    <body>
        <div class="container-fluid">
            <?php include_once("../../layout/header.php"); ?>

            <main>
                <div class="m-container">
                    <div class="m-info">
                        <div class="m-info-title">
                            <h1>Cocinas</h1>
                        </div>
                    </div>
                    <div class="m-gallery">
                        <div class="m-gal-sub1">
                            <div class="m-sub-description">
                                <h3>Cocina 32</h3>
                                <p>En una casa ya construída ya hace varios años, nos solicitaron cambiar de lugar la cocina, así que se tuvo que modificar totalmente ese espacio, desde tapiar la ventana hasta la construcción de las barras, recubrimiento con azulejos y trabajos de carpintería. Aquí las imágenes del antes y el después.</p>
                            </div>
                            <div class="m-sub-img">
                                <img class="img2-1" src="img_a2.1" alt="album2-image1">
                                <img class="img2-2" src="img_a2.2" alt="album2-image2">
                            </div>
                        </div>

                        <div class="m-gal-sub2">
                            <div class="m-sub-description">
                                <h3>Cocina antes y después</h3>
                                <p>Este trabajo se ejecutó en un condominio de playa para ampliar la cocina. La barra principal se alargó hasta la mitad de la puerta corrediza con el fin de que la barra también tuviera una utilidad en la zona del balcón. Los muros y barras se recubrieron con azulejo de barro, se construyó una columna para sostener los arcos que enclaustrarían este espacio, se colocaron unos troncos de madera cerca del techo para bajar un poco ese nivel ya que era demasiado alto. En la puerta de entrada se construyó un arco y se introdujo nueva iluminación en entrada y cocina.</p>
                            </div>
                            <div class="m-sub-img">
                                <img class="img2-3" src="img_a2.3" alt="album2-image3">
                                <img class="img2-4" src="img_a2.4" alt="album2-image4">
                            </div>
                        </div>

                        <div class="m-gal-sub3">
                            <div class="m-sub-description">
                                <h3>Cocina rústica</h3>
                                <p>En esta casa de más de 200 años de edad se decoró el espacio destinado a la cocina. Se limpiaron pisos con manguera y agua a presión, se pintaron muros y barras, se colocaron muebles y elementos decorativos como cuadros y artesanía de barro sobre los muros para darle el característico toque mexicano.</p>
                            </div>
                            <div class="m-sub-img">
                                <img class="img2-5" src="img_a2.5" alt="album2-image5">
                                <img class="img2-6" src="img_a2.6" alt="album2-image6">
                            </div>
                        </div>

                        <div class="m-gal-sub4">
                            <div class="m-sub-description final-desc">
                                <h3>Recubrimientos en cocina</h3>
                                <p>Aquí en esta cocina nuestro cliente tenía una “isla” construída en varios niveles. La diferencia de niveles es un factor que crea separaciones, así que la isla era poco útil. Nos pidió que recubriéramos las barras y la isla con un granito importado de Brasil. La estructura de la isla la tuvimos que adaptar para poder recibir la enorme pieza de piedra. Posteriormente se instaló la tarja y la parrilla eléctrica.</p>
                            </div>
                            <div class="m-sub-img">
                                <img class="img2-7" src="img_a2.7" alt="album2-image7">
                                <img class="img2-8" src="img_a2.8" alt="album2-image8">
                                <img class="img2-9" src="img_a2.9" alt="album2-image9">
                                <img class="img2-10" src="img_a2.10" alt="album2-image10">
                            </div>
                        </div>
                    </div>
                </div>
            </main>

            <?php include_once("../../layout/footer.php"); ?>

            <div id="img-viewer" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <img class="img-modal" src="" alt="image-display" style="width: 100%">
                    </div>
                </div>
            </div>
        </div>
        <?php include_once("../../layout/scripts.php"); ?>
        <script src="sc_album2"></script>
    </body>
</html>