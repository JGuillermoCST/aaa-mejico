<!DOCTYPE html>

<html lang="es">
    <head>
        <?php include_once("../../layout/tags.php"); ?>
        <meta name="title" content="AAA méjico">
        <meta name="description" content="AAA Méjico es una empresa Inmobiliaria que se dedica al asesoramiento en la compra y venta de Bienes Raíces operando desde 1996">

        <?php include_once("../../layout/stylesheets.php"); ?>
        <link rel="stylesheet" href="st_album6">

        <title>Álbum 6</title>
    </head>
    <body>
        <div class="container-fluid">
            <?php include_once("../../layout/header.php"); ?>

            <main>
                <div class="m-container">
                    <div class="m-info">
                        <div class="m-info-title">
                            <h1>Interiores</h1>
                        </div>
                    </div>
                    <div class="m-gallery">
                        <div class="m-gal-sub1">
                            <div class="m-sub-description">
                                <h3>Construcción de chimenea</h3>
                                <p>En este espacio nuestro cliente solicitó una chimenea en ladrillo aparente en una casa de campo. Aquí se muestran las imágenes del antes y el después.</p>
                            </div>
                            <div class="m-sub-img">
                                <img class="img6-1" src="img_a6.1" alt="album6-image1">
                                <img class="img6-2" src="img_a6.2" alt="album6-image2">
                            </div>
                        </div>

                        <div class="m-gal-sub2">
                            <div class="m-sub-description">
                                <h3>Baño</h3>
                                <p>En la primera imagen vemos la barra y preparación para recibir los dos ovalines para este baño. En el muro de la izquierda se instalará un espejo y en el vano de la derecha se instala una ventana con rectánculos de madera y vidrio opaco. Los ovalines de porcelana, accesorios HELVEX y recubrimientos en barras y techos de caoba. En los muros se instalaron losetas rectangulares de porcelanato imitación mármol usando una junta del mismo color de las losetas. Imágenes del antes y el después.</p>
                            </div>
                            <div class="m-sub-img">
                                <img class="img6-3" src="img_a6.3" alt="album6-image3">
                                <img class="img6-4" src="img_a6.4" alt="album6-image4">
                            </div>
                        </div>

                        <div class="m-gal-sub3">
                            <div class="m-sub-description">
                                <h3>Vestidor</h3>
                                <p>En esta remodelación se tuvieron que redondear los vanos de puertas, cambio de pisos, iluminación, colocación de luna, pintura y decoración. Aquí el antes y después.</p>
                            </div>
                            <div class="m-sub-img">
                                <img class="img6-5" src="img_a6.5" alt="album6-image5">
                                <img class="img6-6" src="img_a6.6" alt="album6-image6">
                            </div>
                        </div>

                        <div class="m-gal-sub4">
                            <div class="m-sub-description final-desc">
                                <h3>Recámara</h3>
                                <p>En este caso se cambió la fuente de luz de la habitación. En donde se encontraba una ventana se tapió con block y del lado derecho se semolió el muro para tener la vista del patio. En el espacio donde se encuentra la columna de carga se aprovechó para construir un arco que serviría como cabecera y ahí empotrar la cama. Al final se amuebló con todas las piezas necesarias hasta las cortinas y el toldo para el patio.</p>
                            </div>
                            <div class="m-sub-img">
                                <img class="img6-7" src="img_a6.7" alt="album6-image7">
                                <img class="img6-8" src="img_a6.8" alt="album6-image8">
                                <img class="img6-9" src="img_a6.9" alt="album6-image9">
                            </div>
                        </div>
                    </div>
                </div>
            </main>

            <?php include_once("../../layout/footer.php"); ?>

            <div id="img-viewer" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <img class="img-modal" src="" alt="image-display" style="width: 100%">
                    </div>
                </div>
            </div>
        </div>
        <?php include_once("../../layout/scripts.php"); ?>
        <script src="sc_album6"></script>
    </body>
</html>