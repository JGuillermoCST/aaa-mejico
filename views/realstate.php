<!DOCTYPE html>

<html lang="es">
<head>
        <?php include_once("../layout/tags.php"); ?>
        <meta name="title" content="Contacto AAA méjico">
        <meta name="description" content="AAA Méjico es una empresa Inmobiliaria que se dedica al asesoramiento en la compra y venta de Bienes Raíces operando desde 1996">

        <?php include_once("../layout/stylesheets.php"); ?>
        <link rel="stylesheet" href="st_rstate">

        <title>Inmobiliraria</title>
</head>
<body>
    <div class="container-fluid">
        <?php include_once("../layout/header.php"); ?>

        <main>
            <div class="m-container">
                <div class="m-top">
                    <h1>AAA Inmobiliaria</h1>
                    <p>“AAA Méjico” es una empresa Inmobiliaria que se dedica al asesoramiento en la compra y venta de Bienes Raíces operando desde 1996.<br><br>“AAA Méjico” ofrece también el servicio de construcción y remodelación a través de nuestra filial “Arquitectura Antigua de las Américas” tanto para adaptar los inmuebles a las necesidades del adquiriente como para proyectos independientes.<br><br>“AAA Méjico” tiene una excelente reputación por su honestidad, integridad y conocimiento del mercado inmobiliario. La experiencia de AAA Méjico es conocida por la comunidad local y por residentes extranjeros.<br><br>Agradecemos nos contacte para proporcionarle información sobre nuestros inventarios, sobre todo en el Centro de Guadalajara.</p>
                </div>
                <div class="m-form">
                    <form id="quotation" class="row g-3" method="POST">
                        <div class="col-md-6">
                            <label for="i-name" class="form-label">Nombre completo</label>
                            <input type="text" class="form-control" id="i-name" placeholder="Juan Pérez Ramírez" required>
                        </div>
                        <div class="col-md-6">
                            <label for="i-email" class="form-label">E-Mail</label>
                            <input type="email" class="form-control" id="i-email" placeholder="correo@ejemplo.com" required>
                        </div>
                        <div class="col-md-4">
                            <label for="i-kind" class="form-label">Asunto</label>
                            <select id="i-kind" class="form-select">
                            <option value="select" selected>Seleccionar</option>
                            <option value="Compra">Compra</option>
                            <option value="venta">Venta</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label for="i-area" class="form-label">Área de la ciudad</label>
                            <input type="text" class="form-control" id="i-area" placeholder="Centro" required>
                        </div>
                        <div class="col-md-4">
                            <label for="i-range" class="form-label">Rango de precio</label>
                            <select id="i-range" class="form-select">
                            <option value="select" selected>Seleccionar</option>
                            <option value="50,000-100,000 USD">50,000 - 100,000 USD</option>
                            <option value="100,000-250,000 USD">100,000 - 250,000 USD</option>
                            <option value="250,000-500,000 USD">250,000 - 500,000 USD</option>
                            <option value="500,000-1,000,000 USD">500,000 - 1,000,000 USD</option>
                            <option value="1000+ USD">1,000,000+ USD</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <button type="submit" class="btn btn-primary">Enviar datos</button>
                        </div>
                    </form>
                </div>
            </div>
        </main>

        <?php include_once("../layout/footer.php"); ?>
    </div>
    <?php include_once("../layout/scripts.php"); ?>
    <script src="sc_rstate"></script>
</body>
</html>