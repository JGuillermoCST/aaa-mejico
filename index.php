<!DOCTYPE html>

<html lang="es">
    <head>
        <?php include_once("layout/tags.php"); ?>
        <meta name="title" content="AAA méjico">
        <meta name="description" content="AAA Méjico es una empresa Inmobiliaria que se dedica al asesoramiento en la compra y venta de Bienes Raíces operando desde 1996">

        <?php include_once("layout/stylesheets.php"); ?>
        <link rel="stylesheet" href="st_index">
        
        <title>Inicio</title>
    </head>
    <body>
        <div class="container-fluid">
            <?php include_once("layout/header.php"); ?>

            <main>
                <div class="m-container">
                    <img src="img_banner" alt="Main banner" oncontextmenu="return false">
                </div>
            </main>

            <?php include_once("layout/footer.php"); ?>
        </div>
        <?php include_once("layout/scripts.php"); ?>
        <script src="sc_general"></script>
        <script src="sc_index"></script>
    </body>
</html>